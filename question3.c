#include <stdio.h>
int main(){
	int n1,n2,swap;
	printf("Enter number one : ");
	scanf("%d\n", &n1);
	printf("Enter number two : ");
	scanf("%d\n", &n2);
	swap=n1;
	n1=n2;
	n2=swap;
	printf(" After swapping new number one = %d\n", n1);
	printf("After swapping new number two = %d\n", n2);
	return 0;
}
